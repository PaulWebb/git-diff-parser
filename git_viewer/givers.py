from urllib.request import Request, urlopen
import re
from django.core.files.uploadedfile import UploadedFile

GITHUB_COMMIT_URL = re.compile(
    r'^https?:\/\/(?:www\.)?github\.com\/(.*?)\/(.*?)\/commit\/(.*?)(?:\.diff)?(?:\.patch)?(?:\/.*)?$')
GITHUB_PR_URL = re.compile(
    r'^https?:\/\/(?:www\.)?github\.com\/(.*?)\/(.*?)\/pull\/(.*?)(?:\.diff)?(?:\.patch)?(?:\/.*)?$')
GITLAB_COMMIT_URL = re.compile(
    r'^https?:\/\/(?:www\.)?gitlab\.com\/(.*?)\/(.*?)\/commit\/(.*?)(?:\.diff)?(?:\.patch)?(?:\/.*)?$')
GITLAB_PR_URL = re.compile(
    r'^https?:\/\/(?:www\.)?gitlab\.com\/(.*?)\/(.*?)\/merge_requests\/(.*?)(?:\.diff)?(?:\.patch)?(?:\/.*)?$')
BITBUCKET_COMMIT_URL = re.compile(
    r'^https?:\/\/(?:www\.)?bitbucket\.org\/(.*?)\/(.*?)\/commits\/(.*?)(?:\/raw)?(?:\/.*)?$')
BITBUCKET_PR_URL = re.compile(r'^https?:\/\/(?:www\.)?bitbucket\.org\/(.*?)\/(.*?)\/pull-requests\/(.*?)(?:\/.*)?$')


def github_req_generator(user_name,project_name, url_type, value):
    req = Request('https://api.github.com/repos/{}/{}/{}/{}'.format(user_name,project_name, url_type, value))
    req.add_header('Accept', 'application/vnd.github.v3.diff')
    return req


def bitbucket_req_generator(user_name, project_name, type, value):
    base_url = 'https://bitbucket.org/api/2.0/repositories/';
    if type == 'pullrequests':
        return Request("{}{}/{}/pullrequests/{}/diff".format(base_url,user_name,project_name, value))

    return Request("{}{}/{}/diff/{}".format(base_url,user_name,project_name, value))


def gitlab_req_generator(user_name, project_name, url_type, value):
    return Request('https://crossorigin.me/https://gitlab.com/{}/{}/{}/{}.diff'.
                   format(user_name,project_name, url_type, value))


URL_CHECKER = [
    (GITHUB_COMMIT_URL, github_req_generator, 'commits'),
    (GITHUB_PR_URL, github_req_generator, 'pulls'),
    (GITLAB_COMMIT_URL, gitlab_req_generator, 'commit'),
    (GITLAB_PR_URL, gitlab_req_generator, 'merge_requests'),
    (BITBUCKET_COMMIT_URL, bitbucket_req_generator, 'commit'),
    (BITBUCKET_PR_URL, bitbucket_req_generator, 'pullrequests'),
]

def give_text_from_uploaded_file(uploaded_file: UploadedFile, encoding: str = "utf-8") -> str:
    if not uploaded_file:
        return None
    return str(uploaded_file.file.read(), encoding)


def give_text_from_url(url: str, req_timeout=10):
    if not url:
        return None
    data = None
    req = Request(url)
    for checker, gen, url_type in URL_CHECKER:
        groups = checker.match(url)
        if not groups:
            continue
        req = gen(groups.group(1), groups.group(2), url_type, groups.group(3))
        break
    with urlopen(req, timeout=req_timeout) as file:
        data = file.read()
    return str(data, "utf-8")
