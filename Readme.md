# Git diff parser

[DEMO STAND](http://ec2-52-14-126-231.us-east-2.compute.amazonaws.com/)

generates pretty HTML diffs from git or unified diff output.

## Specifications
[git diff official document](https://git-scm.com/docs/git-diff)

[git diff atlassian](https://www.atlassian.com/git/tutorials/saving-changes/git-diff)

[Unified Diff Format](https://www.artima.com/weblogs/viewpost.jsp?thread=164293)

[How to read the output from git diff?](https://stackoverflow.com/questions/2529441/how-to-read-the-output-from-git-diff)



## How to run


```bash
manage.py runserver
```

but before
```bash
pip install -r requirements.txt
```
and dont forget about virtual environment

run tests

```bash
manage.py test diff_parser.tests
```
Samples data you can find in samples folder.

```bash
diff --git a/README b/README
index bccdfbd..b0ed415 100644
--- a/README
+++ b/README
@@ -1 +1,2 @@
This is the README file.
+One more line.
```
## Sources

- BOX - just put data to textarea box
- URL - example https://bitbucket.org/PaulWebb/git-diff-parser/commits/b61874c560f21f3538f9e824f56602ed1a31c463
support:
    - GITHUB commits and pull requests
    - BITBUCKET commits and pull requests
    - GILAB commits and pull requests
- FILE - open a file or drag and drop it





## Engines

[What The Patch!?](https://github.com/cscorley/whatthepatch)

[Unidiff](https://github.com/matiasb/python-unidiff)

## Docker

```bash
cd docker
docker-compose build
docker-compose up -d
```
 
 for production:
 
```bash

python manage.py collectstatic
cd docker
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
```

[open this link](http://localhost:80)



