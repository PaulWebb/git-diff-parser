(function( $ ){

  $.fn.diffParser = function( options ) {

    return this.each(function() {
        $that = $(this);
        // selct name mode activate
        $("select[name=mode]",$that).change(function () {
            var item = ".s"+$(this).val();
            $(".source",$that).hide();
            $(".source :input",$that).removeAttr("required");
            $(item ,$that).show();
            $(item ,$that).removeClass("d-none");
            $(item+" :input",$that).attr("required", "required");
            $(".diffs").empty();
        });
        $("select[name=mode]",$that).trigger("change");

        $(this).submit(function(e){
            e.preventDefault();

            if(!this.checkValidity()){
                alert("Please fill out all required fields marked in red");
                return;
            }
            var $btn = $("button",$(this));
            $btn.prop("disabled", true);
            var formData = new FormData($that.get(0));
            var boxData = $.base64.encode(formData.get('data'));
            formData.set('data', boxData);
            $.ajax({
               type: "POST",
               url: "/parse",
               data: formData,
                cache: false,
                processData: false,
                contentType: false,
               success: function(data)
               {
                   $(".diffs").html(data);
               },
                error: function(xhr){
                        alert(xhr.responseText);
                        $(".diffs").empty();
                },
                complete: function () {
                    $btn.prop("disabled", false);
                }
             });
        });

    });

  };
})( jQuery );

$(document).ready(function () {
    $("form").diffParser();
});