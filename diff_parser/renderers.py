from bootstrap4.renderers import FieldRenderer
from bootstrap4.utils import add_css_class


class DiffParserFieldRenderer(FieldRenderer):

    def get_form_group_class(self):
        form_group_class = self.widget.attrs.get("form_group_class", self.form_group_class)
        if self.field.errors:
            if self.error_css_class:
                form_group_class = add_css_class(form_group_class, self.error_css_class)
        else:
            if self.field.form.is_bound:
                form_group_class = add_css_class(form_group_class, self.success_css_class)
        if self.field.field.required and self.required_css_class:
            form_group_class = add_css_class(form_group_class, self.required_css_class)
        if self.layout == "horizontal":
            form_group_class = add_css_class(form_group_class, "row")
        return form_group_class
