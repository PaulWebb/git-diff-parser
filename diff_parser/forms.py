from django import forms
from django.utils.translation import ugettext_lazy as _
from diff_parser.models import Sources
from diff_parser.parsers import Parser
import logging
from git_viewer.givers import give_text_from_uploaded_file, give_text_from_url
import base64

logger = logging.getLogger(__name__)

class ParserForm(forms.Form):
    mode = forms.TypedChoiceField(widget=forms.Select(attrs={'form_group_class': 'form-group col-md-6'}),
                                  choices=Sources.choices(), label=_("Source"), coerce=int)
    engine = forms.TypedChoiceField(widget=forms.Select(attrs={'form_group_class': 'form-group col-md-6'}),
                               choices=Parser.choices(), label=_("Engine"), coerce=int)

    data = forms.CharField(label=_("Data"), widget=forms.Textarea(attrs={'rows': 4, 'cols': 5, 'form_group_class':
        'form-group col-md-12 source s{}'.format(Sources.BOX.value)}), required=False)
    url = forms.URLField(label=_("URL"), widget=forms.URLInput(attrs={'form_group_class':
                                                                          'form-group col-md-12 d-none source s{}'.format(
                                                                              Sources.URL.value)}), required=False)
    file = forms.FileField(label=_("File"), widget=forms.ClearableFileInput(attrs={'form_group_class':
                                                                                       'form-group col-md-12 d-none source s{}'.format(
                                                                                           Sources.FILE.value)}),
                           required=False)

    class Media:
        js = ('diff_parser.client.js',)
        css = {
            'all': ('diff_parser.css',)
        }

    def extract(self) -> str:
        if not self.is_valid():
            return None
        self.mode = self.cleaned_data['mode']
        self.engine = self.cleaned_data['engine']
        result = None
        try:
            if self.mode == Sources.BOX:
                result = str(base64.b64decode(self.cleaned_data.get("data", None)),"utf-8")
            elif self.mode == Sources.FILE:
                result = give_text_from_uploaded_file(self.cleaned_data.get("file", None))
            else:
                result = give_text_from_url(self.cleaned_data.get("url", None))
        except Exception as ex:
            logger.exception(_("cant get data from source"))

        return result
