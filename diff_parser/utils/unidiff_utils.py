from unidiff import PatchedFile, Hunk
from ..models import Tags, LineType


def is_renamed(patch: PatchedFile) -> bool:
    return (patch.source_file.startswith('a/') and
            patch.target_file.startswith('b/')) and patch.source_file[2:] != patch.target_file[2:]


def get_head(patch: PatchedFile) -> str:
    '''
    return path abstracted from VCS for PatchedFile
    :param patch: PatchedFile
    :return: path
    '''
    if is_renamed(patch):
        filepath = "{} -> {}".format(patch.source_file[2:], patch.target_file[2:])
    elif (patch.source_file.startswith('a/') and
          '/dev/null' in patch.target_file):
        filepath = patch.source_file[2:]
    elif (patch.target_file.startswith('b/') and
          '/dev/null' in patch.source_file):
        filepath = patch.target_file[2:]
    else:
        filepath = patch.source_file
    return filepath


def get_tags(patch: PatchedFile) -> (Tags,):
    tags = []
    if patch.is_added_file:
        tags.append(Tags.ADDED)
    elif patch.is_removed_file:
        tags.append(Tags.REMOVED)
    elif patch.is_modified_file:
        tags.append(Tags.MODIFIED)
    if is_renamed(patch):
        tags.append(Tags.RENAMED)
    return (*tags,)


def get_label(hunk: Hunk) -> str:
    return "@@ -%d,%d +%d,%d @@%s\n" % (
        hunk.source_start, hunk.source_length,
        hunk.target_start, hunk.target_length,
        ' ' + hunk.section_header if hunk.section_header else '')


def get_line_type(line) -> LineType:
    if line.is_added:
        return LineType.ADDED
    elif line.is_removed:
        return LineType.REMOVED
    else:
        return LineType.COMMON


def decompose(patch):
    return get_head(patch), get_tags(patch)
