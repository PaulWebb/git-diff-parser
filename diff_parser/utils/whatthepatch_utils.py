from whatthepatch.patch import unified_hunk_start, Change
from whatthepatch.snippets import split_by_regex
import re
from ..models import Tags, LineType

unified_change = re.compile('^([-+ ]|)(.*)$')

def is_renamed(patch_header) -> bool:

    return '/dev/null' not in patch_header.old_path and '/dev/null' not in patch_header.new_path and patch_header.old_path != patch_header.new_path


def get_head(patch) -> str:
    """
    return path abstracted from VCS for PatchedFile
    :param patch: PatchedFile
    :return: path
    """

    if is_renamed(patch.header):
        filepath = "{} -> {}".format(patch.header.old_path, patch.header.new_path)
    elif '/dev/null' in patch.header.new_path:
        filepath = patch.header.old_path
    elif '/dev/null' in patch.header.old_path:
        filepath = patch.header.new_path
    else:
        filepath = patch.header.old_path
    return filepath


def get_tags(patch) -> (Tags,):
    tags = []
    if '/dev/null' in patch.header.old_path:
        tags.append(Tags.ADDED)
    elif '/dev/null' in patch.header.new_path:
        tags.append(Tags.REMOVED)
    else:
        tags.append(Tags.MODIFIED)
    if is_renamed(patch.header):
        tags.append(Tags.RENAMED)
    if not patch.changes:
        tags.append(Tags.BINARY)
    return (*tags,)

def get_line_type(line) -> LineType:
    if line.old is None:
        return LineType.ADDED
    elif line.new is None:
        return LineType.REMOVED
    else:
        return LineType.COMMON


def decompose(patch):
    return get_head(patch), get_tags(patch)

def parse_unified_diff(text):

    try:
        lines = text.splitlines()
    except AttributeError:
        lines = text

    old = 0
    new = 0
    r = 0
    i = 0

    changes = list()

    hunks = split_by_regex(lines, unified_hunk_start)
    for hunk_n, hunk in enumerate(hunks):
        # reset counters
        r = 0
        i = 0
        hunk_label = None

        while len(hunk) > 0:
            h = unified_hunk_start.match(hunk[0])

            del hunk[0]
            if h:
                hunk_label = h.group(0)
                old = int(h.group(1))
                if len(h.group(2)) > 0:
                    old_len = int(h.group(2))
                else:
                    old_len = 0

                new = int(h.group(3))
                if len(h.group(4)) > 0:
                    new_len = int(h.group(4))
                else:
                    new_len = 0

                h = None
                break
        hunk_info = (hunk_n, hunk_label)
        while len(hunk) > 0:
            c = unified_change.match(hunk[0])

            if c:
                kind = c.group(1)
                line = c.group(2)
                c = None

                if kind == '-' and (r != old_len or r == 0):
                    changes.append(Change(old + r, None, line, hunk_info))
                    r += 1
                elif kind == '+' and (i != new_len or i == 0):
                    changes.append(Change(None, new + i, line, hunk_info))
                    i += 1
                else:
                    changes.append(Change(old + r, new + i, line, hunk_info))
                    r += 1
                    i += 1

            del hunk[0]

    if len(changes) > 0:
        return changes

    return None