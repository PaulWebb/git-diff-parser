from enum import Enum

from git_viewer.utils import ChoiceEnum
from collections import namedtuple


class Sources(ChoiceEnum):
    """
    list of data sources
    """
    BOX = 1
    FILE = 2
    URL = 3


class Tags(ChoiceEnum):
    ADDED = 1
    MODIFIED = 2
    REMOVED = 4
    RENAMED = 8
    BINARY = 16


class LineType(ChoiceEnum):
    ADDED = 1
    REMOVED = 2
    COMMON = 3

    def __str__(self):
        if self == LineType.ADDED:
            return "+"
        elif self == LineType.REMOVED:
            return "-"
        else:
            return ""


patch = namedtuple('patch', 'header tags chunks')
chunk = namedtuple('chunk', "label lines")
line = namedtuple('line', "old_num new_num type value")
