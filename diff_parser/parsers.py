from git_viewer.utils import MetaSingleton
from django.utils.translation import ugettext_lazy as _
from git_viewer.utils import uncache
import unidiff.constants
import re

from . import models
from .utils import unidiff_utils as uu, whatthepatch_utils as wu

'''
monkey patching
specifications?
how to define a common line?
'''
unidiff.constants.RE_HUNK_BODY_LINE = re.compile(
    r'^(?P<line_type>([- \+\\]|))(?P<value>.*)', re.DOTALL)

uncache(['unidiff.constants'])
from unidiff import PatchSet
from unidiff.errors import UnidiffParseError

import whatthepatch

"""
monkey patching
specifications? 
how to define a common line?
"""

whatthepatch.patch.parse_unified_diff = wu.parse_unified_diff


class ParseException(Exception):
    pass


class Parser(object):
    registry = []
    label = ''
    order = 0

    def __init_subclass__(cls, label='some', order=0):
        super().__init_subclass__()
        cls.order = order
        cls.label = label
        cls.registry.append((cls.label, cls, cls.order))
        cls.registry.sort(key=lambda item: item[2])

    @classmethod
    def parse(cls, data: str):
        def parser(ind):
            if ind >= len(cls.registry):
                return None
            return cls.registry[ind][1].parse(data)

        return parser

    @classmethod
    def choices(cls):
        return ((i, x[0]) for i, x in enumerate(cls.registry))


class UnifiedParser(Parser, label=_('Unified Parser'), metaclass=MetaSingleton):

    @classmethod
    def parse(cls, data: str):
        patches = []
        try:
            patch_set = PatchSet.from_string(data)
            for tp_patch in patch_set:
                chunks = []
                patches.append(models.patch(*uu.decompose(tp_patch), chunks))
                for tp_chunk in tp_patch:
                    lines = []
                    chunks.append(models.chunk(uu.get_label(tp_chunk), lines))
                    for tp_line in tp_chunk:
                        lines.append(models.line(tp_line.source_line_no, tp_line.target_line_no,
                                                 uu.get_line_type(tp_line), tp_line.value))
        except UnidiffParseError as ex:
            raise ParseException("parse exception: {}".format(str(ex))) from ex
        return patches


class WTPParser(Parser, label=_('What The Patch!?'), order=-1, metaclass=MetaSingleton):

    @classmethod
    def parse(cls, data: str):
        patches = []
        try:
            for tp_patch in whatthepatch.parse_patch(data):
                chunks = []
                patches.append(models.patch(*wu.decompose(tp_patch), chunks))
                i = 0
                if not tp_patch.changes:
                    continue
                for item in tp_patch.changes:
                    if i != item.hunk:
                        lines = []
                        if isinstance(item.hunk, tuple):
                            chunks.append(models.chunk(item.hunk[1], lines))
                        else:
                            chunks.append(models.chunk("chunk {}".format(item.hunk), lines))
                        i = item.hunk
                    lines.append(models.line(item.old, item.new, wu.get_line_type(item), item.line))
        except Exception as ex:
            raise ParseException("parse exception: {}".format(str(ex))) from ex
        return patches
