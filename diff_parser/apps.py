from django.apps import AppConfig


class DiffParserConfig(AppConfig):
    name = 'diff_parser'
