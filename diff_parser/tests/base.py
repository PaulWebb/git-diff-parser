from django.test import TestCase
import os
from git_viewer.settings import SITE_ROOT
import base64

def load_sample(name, ext='txt', encode=False):
    """
    get sample from file which stores in samples folder
    :param name: file name without extension
    :param ext: extension
    :return: data from file
    """
    with open(os.path.join(SITE_ROOT, '../../samples/', "%s.%s" % (name, ext)), 'r') as file:
        data = file.read()
    if encode:
        return base64.b64encode(data.encode('utf-8')).decode('utf-8')
    return data


class BaseTestCase(TestCase):
    databases = []

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass
