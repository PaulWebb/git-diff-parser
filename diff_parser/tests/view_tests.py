from diff_parser.models import Sources
from diff_parser.parsers import Parser
from .base import BaseTestCase, load_sample
from git_viewer.settings import SITE_ROOT
import os

# Create your tests here.


class DiffParserTest(BaseTestCase):



    def test_parse_view(self):
        parsers = list(Parser.choices())
        #WTPParser
        resp = self.client.post('/parse', {'mode':Sources.BOX.value,
                                           'engine': parsers[0][0],
                                           'data':load_sample('test', encode=True)})

        self.assertEqual(resp.status_code, 200)
        self.assertRegex(resp.content.decode('utf-8'), r'card')
        self.assertRegex(resp.content.decode('utf-8'), r'MODIFIED')
        #UnifiedParser
        resp = self.client.post('/parse', {'mode': Sources.BOX.value,
                                           'engine': parsers[1][0],
                                           'data': load_sample('test', encode=True)})

        self.assertEqual(resp.status_code, 200)
        self.assertRegex(resp.content.decode('utf-8'), r'card')
        self.assertRegex(resp.content.decode('utf-8'), r'MODIFIED')



    def test_parse_view_wrong_data(self):
        resp = self.client.post('/parse', {'data':'test'})

        self.assertEqual(resp.status_code, 400)
        #self.assertRegex(resp.content.decode('utf-8'), r'alert')