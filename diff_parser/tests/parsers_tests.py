from django.test import TestCase

from diff_parser.models import Tags, LineType
from .base import BaseTestCase, load_sample
from ..parsers import WTPParser, Parser


class ParserTestCase(BaseTestCase):

    def test_parser(self):
        parsers = Parser.registry
        self.assertEqual(2, len(parsers))
        self.assertEqual('What The Patch!?', parsers[0][0])

    def test_wtp_parser(self):
        data = load_sample("test")
        patches = WTPParser.parse(data)
        self.assertEqual(1, len(patches))
        patch = patches[0]
        self.assertEqual("README", patch.header)
        self.assertTrue(Tags.MODIFIED in patch.tags)
        self.assertEqual(1, len(patch.chunks))
        chunk = patch.chunks[0]
        self.assertEqual(2, len(chunk.lines))
        line1 = chunk.lines[0]
        self.assertEqual(LineType.COMMON, line1.type)
        self.assertEqual("This is the README file.", line1.value)
        line2 = chunk.lines[1]
        self.assertEqual(LineType.ADDED, line2.type)
        self.assertEqual("One more line.", line2.value)

    def test_wtp_parser_binary(self):
        data = load_sample("test_binary")
        patches = WTPParser.parse(data)
        self.assertEqual(2, len(patches))
        patch2=patches[1]
        self.assertTrue(Tags.BINARY in patch2.tags)

    def test_wtp_parser_rename(self):
        data = load_sample("test_rename")
        patches = WTPParser.parse(data)
        self.assertEqual(1, len(patches))
        patch = patches[0]
        self.assertTrue(Tags.RENAMED in patch.tags)

    def test_wtp_parser_huge(self):
        data = load_sample("test_huge")
        patches = WTPParser.parse(data)
        self.assertEqual(12, len(patches))

    def test_wtp_parser_last_lines(self):
        """
        last lines can be empty in chunks
        :return:
        """
        data = load_sample("test_last_lines")
        patches = WTPParser.parse(data)
        print(patches)
        self.assertEqual(1, len(patches))