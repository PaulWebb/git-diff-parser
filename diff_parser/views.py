from django.shortcuts import render
from django.http import HttpResponseBadRequest
from django.views.decorators.http import require_POST
from .forms import ParserForm
from .parsers import Parser, ParseException
from django.utils.translation import ugettext_lazy as _




def index(request):
    form = ParserForm()
    return render(request, 'index.html', {'form': form})


@require_POST
def parse(request):
    """
    convert git diff to html
    :param request: ParserForm
    :return: html or error
    """
    form = ParserForm(request.POST, request.FILES)
    data = form.extract()
    if data:
        try:
            patches = Parser.parse(data)(form.engine)
            if not patches:
                raise ParseException("Data doesn't match  specifications.")
            return render(request, 'patches.html', {'patches': patches})
        except ParseException as ex:
            return render(request, 'error.html', {'msg': str(ex)})
    return HttpResponseBadRequest("problem with source")
